import speech_recognition as sr
import yagmail

# Speech to Text

recognizer=sr.Recognizer()
with sr.Microphone() as source:
    print("=====Clearing background noise..======")
    recognizer.adjust_for_ambient_noise(source, duration=1)
    print("======Say something! I'm recording your voice...======")
    recordedaudio=recognizer.listen(source)
    print("======Done Recording..!======")

    try:
        print("======Printing Your Message..======")
        text=recognizer.recognize_google(recordedaudio, language="en-US")

        print('Your Message: {}'.format(text))

    except Exception as ex:
        print(ex)
    
# Automate Email

receiver = 'ashiqur49.rahaman@gmail.com'
message=text
sender=yagmail.SMTP('ardipta82@gmail.com')
try:
    sender.send(to=receiver, subject="This is an automate email", contents=message)
    print("======Successfully Send Message! Check your mailbox======")
except Exception as ex:
    print(ex)
