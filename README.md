# README #

### Packages need to install ###

* SpeechRecognition
* pipwin
* pyaudio
* yagmail
* keyring

### Packages installation command (Global Install) ###

* pip install SpeechRecognition
* pip install pipwin
* pipwin install pyaudio
* pip install yagmail
* pip install keyring

### Run guide ###

* connect to microphone
* change receiver email address
* change sender email address
* you must "Less secure app access" is on for your sender mail account.
